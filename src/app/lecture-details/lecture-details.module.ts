import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LectureDetailsPageRoutingModule } from './lecture-details-routing.module';

import { LectureDetailsPage } from './lecture-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LectureDetailsPageRoutingModule
  ],
  declarations: [LectureDetailsPage]
})
export class LectureDetailsPageModule {}

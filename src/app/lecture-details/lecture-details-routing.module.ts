import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LectureDetailsPage } from './lecture-details.page';

const routes: Routes = [
  {
    path: '',
    component: LectureDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LectureDetailsPageRoutingModule {}
